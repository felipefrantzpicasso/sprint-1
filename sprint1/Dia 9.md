## Dia 9


# CyberSecurity 🌐 🖥️


# Principais formas de proteção:

• Nunca utilizar a mesma senha para tudo

• Não clicar em sites ou links desconhecidos

• Verificar o caminho de um link recebido


# Security Wi-fi:

- Ataques DNS ⚠️

- Botnets ⚠️

- Ataques DDOS⚠️

Hoje em dia ninguém está 100% protegido de certos ataques, abaixo algumas soluções de como se proteger deles. 

- utilizar firewalls 🛡️

- implementar autenticação de dois fatores 🛡️

- manter o software atualizado 🛡️

- utilizar servidores DNS confiáveis 🛡️

- monitorar atividades suspeitas no tráfego de rede. 🛡️


# Ataque Força Bruta ☠

- O uso ataque força bruta em contextos de segurança nos coloca ao início da computação moderna, com tentativas sistemáticas de decifrar códigos e senhas por meio de repetições exaustivas de todas as combinações possíveis. Por isso é tão necessário utilizar senhas diferentes, complexas e sempre utilizar a autenticação de 2 fatores.

# Geral

Nesse dia pude ter a oportunidade de aprender mais sobre a cyber security que vem tendo muito espaço atualmente por conta de ataques on-line feitos por hackers.




