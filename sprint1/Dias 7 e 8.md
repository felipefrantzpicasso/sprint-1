## Dias 7 e 8

# Java


# Sobre a linguagem: 

• Java é uma linguagem de programação versátil que pode ser executada em diferentes sistemas operacionais e dispositivos. Ela é projetada para seguir os princípios de programação orientada a objetos e possui recursos avançados de rede. Além de ser rápida e eficiente, Java é conhecida por sua segurança e confiabilidade. 


# Principais áreas

• Programação Orientada a Objetos (POO): Java é uma linguagem de programação que segue o paradigma da orientação a objetos. Isso implica que em Java tudo é representado como um objeto. Os objetos são criados a partir de classes, que funcionam como modelos que definem tanto os dados quanto os comportamentos relacionados a eles.

• Segurança: Java tem um sistema de segurança robusto, criado para proteger os usuários contra códigos maliciosos. A Máquina Virtual Java (JVM) executa o código Java em um ambiente chamado "sandbox", que limita o acesso aos recursos do sistema e assegura a execução segura do código.


# Conceitos básicos da linguagem

• Classes e objetos - As classes são responsáveis por estabelecer a estrutura e o comportamento, enquanto os objetos são criados a partir dessas classes.

• Métodos - Pequenos trechos de código que executam ações específicas.

• Dados primitivos - São categorias fundamentais de dados, como números inteiros (int), números decimais (double), valores lógicos (boolean) e caracteres (char).

• Variáveis - Usada para armezanar valores na memória.

• Heranças - Possibilita que uma classe adquira as características de outra classe através de herança.

• Pacotes - Agrupam classes de forma lógica e organizada.

<h1>![java](https://github.com/felipefrantzpicasso/ProjetoGit/assets/134324945/f0d80eab-86ae-40a0-a841-1741d9ae6e51)</h1>
