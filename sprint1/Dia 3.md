## Dia 3

Iniciando o dia 3 seguindo com o aprendizado e aprimoramento sobre o Framework Scrum.


# Entregas Fatias e Incrementais

Cada vez mais, vamos buscando ser mais ágeis com as entregas voltadas ao projeto. Desta forma, fazendo com que as entregas sejam feitas em partes e em um curto período, facilitando cada vez mais o projeto final.


# QA dentro de um time Ágil

O papel do QA é fundamental na equipe para garantir a entrega do projeto sem imprevistos, sendo sempre importante que todos os membros da equipe sejam versáteis em suas funções. Em meio a todo esse processo ágil, o QA está presente em todas as fases do projeto, sempre trazendo benefícios para o time e buscando ser cada vez mais ágil.


# Geral

Concluindo o dia 3, pude aprimorar o aprendizado sobre a metodologia ágil com o framework Scrum. Também pude ver como o QA funciona nas Sprints e a importância das entregas fatias, para que o projeto sempre se encaminhe o mais ágil possível para o produto final.