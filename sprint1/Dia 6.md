## Dia6


# O Princípio de Pareto

• Ao aplicar o princípio de Pareto à avaliação da qualidade, a equipe pode maximizar seus esforços, concentrando-se nas áreas mais significativas para garantir a satisfação do usuário e a eficiência no processo de desenvolvimento.

• O princípio de Pareto, conhecido como regra 80/20, é uma observação baseada em experiências que estabelece que cerca de 80% dos efeitos são atribuídos a apenas 20% das causas.

<h1> ![graph](https://github.com/felipefrantzpicasso/ProjetoGit/assets/134324945/a3348717-c719-41c0-94be-6e39e7881b8d) </h1>


# A regra de Myers

• Os testes de software proporcionam uma série de benefícios, sendo notáveis a diminuição em 70% do índice de retrabalho e a redução em 50% do tempo necessário para aprovar uma nova versão.

<h2>![myers](https://github.com/felipefrantzpicasso/ProjetoGit/assets/134324945/19ccf1bc-45dd-4785-9b25-03793968dd7e) </h2>





