## Dia 2

Iniciando o aprendizado sobre o framework Scrum.

# Scrum

**Tópicos**

Funções de cada integrante

Os 3 Pilares Fundamentais

Dinâmica


# Funções de cada integrante


Scrum Master - Um integrante com amplo conhecimento em Scrum assume a responsabilidade de orientar e auxiliar a equipe.

Product Owner - É a pessoa encarregada de atuar como o cliente, responsável por identificar e estabelecer as necessidades e prioridades.

Time de desenvolvimento - Vai ser a equipe que assumirá a responsabilidade pelo desenvolvimento das funcionalidades do produto, trabalhando de maneira independente e bem coordenada, colaborando entre si para atender às necessidades exigidas da maneira mais eficiente possível.


# Os 3 Pilares Fundamentais

Transparência - Uma comunicação clara e transparente é essencial para garantir o sucesso do projeto, fornecendo informações sobre o desenvolvimento, necessidades e prazos de forma aberta e precisa.

Inspeção - Realizar uma inspeção minuciosa de todas as atividades em andamento.

Adaptação - É essencial estar aberto à adaptação, sendo bastante frequente ocorrer mudanças nos processos e até mesmo no produto, sendo necessário estar disposto a se adequar a essas mudanças.

# Dinâmica

Product Backlog - lista sequencial determinada pelo Product Owner, contendo as funcionalidades, requisitos e necessidades que comporão o produto, organizadas por ordem de prioridade.

Sprint Planning - Reunião entre o Product Owner e o Time de Desenvolvimento, na qual são discutidos e definidos os objetivos e funcionalidades que o produto irá alcançar, além de planejar as tarefas que precisam ser realizadas.

Daily Scrum - Uma reunião diária, coordenada pelo Scrum Master e com a participação do Time de Desenvolvimento, com o objetivo de compartilhar atualizações sobre as atividades realizadas e solucionar as dúvidas do time.

Sprint Review - Reunião final da Sprint, na qual o Time de Desenvolvimento compartilha o trabalho desenvolvido no projeto, recebendo feedback do Product Owner.

Sprint Retrospective - Uma reunião de retrospectiva, na qual o Time de Desenvolvimento e o Scrum Master se reúnem para refletir sobre a Sprint anterior, identificando pontos fortes e possíveis melhorias a serem aplicadas no próximo projeto.

# Geral

Terminando o dia 2 com muito conhecimento sobre Scrum. Com esses conhecimentos pude ver o quão ágil e eficaz é essa metodologia, sempre priorizando a organização, qualidade e evolução do projeto.