## Dia 1

Iniciamos nosso aprendizado com o primeiro dia da Sprint, tivemos nossa primeira reunião onboarding com orientações, explicação de horários e challenges.

# Tópicos

Onboard

Challenges

Matriz de Eisenhower

Gitlab - Github

Readme


# Onboarding

Nessa primeira reunião que tivemos, conhecemos a aquipe da Academy responsável pela parte de hoários e organização do PB. Em seguida, ja com o time montado foi apresentado como vai funcionar a dinâmica do time nos próximos meses.

Challenge

No challenge da Sprint 1, foi solicitado a criação de uma conta no Gitlab e Github para que seja feito um resumo de cada um dos dias de estudo da Sprint por meio de um arquivo.md (Read me).

CONTEÚDOS ABORDADOS

Matriz de Eisenhower

Essa ferramenta pode ser utilizada como um método de classificação de tarefas com base em sua urgência e importância, ajudando a determinar quais devem ser tratadas primeiro.

Gitlab - Github

Foi realizado a criação de contas nas duas plataformas para o uso da  do sistema de versionamento utilizado para hospedar arquivos.md com resumos dos dias. Após a criação de conta foi feito o download da interface GitBash que funciona como um terminal para os comandos GIT:

git status: mostra o status das alterações feitas antes de serem comitadas;

git init: inicia um repositório sem conteúdo;

git add: adiciona um arquivo para a área de espera;

git commit -m "mensagem": manda as alterações que estavam na área de espera;

git push -u origin main: manda as alterações e arquivos comitados para o repositório do Github;

git branch -M main: para mudar o nome da branch principal de master para main;

git remote add origin -link do github-: faz uma coneão do repósitorio local da máquina com o Github;


git checkout -nova branch-: utilizado para criar uma nova branch e também para trocar a branch que vai ser alterada;

git merge -nome da branch-: junta as alterações de uma branch criada com a master ( main );

git clone -link do repósitorio que queira clonar-: faz uma cópia de um repositório do Github para o repositório local selecionado;

# Readme

Readme é um tipo de arquivo.md que segue a linguagem de marcação markdown sendo utilizado para escrever e formatar instruções relacionadas a um projeto.







