## Dia 4

# Como gerar qualidade no produto?

Para garantir a qualidade do produto, é necessário ir além dos testes de software e alcançar os objetivos desejados pelo cliente. Não basta que o produto apenas funcione mecanicamente; é preciso ter um conjunto de elementos essenciais presentes durante todo o processo. Dentro desses elementos podemos citar:


• Requisitos  atendidos;

• Confiança;

• Identificar  defeitos;

• Tomadas de decisão;

• Reduzir riscos;

• Conformidades contratuais e regulatórias.


# Objetivos do teste

• Avaliar produtos;

• Verificar Requisitos;

• Validar objeto do teste;

• Criar confiança;

• Evitar defeitos;

• Encontrar falhas;

• Auxiliar tomada de decisões;

• Reduzir;

• Atender conformidade contratual.


# Teste Estático vs Dinâmico

| Estático | Dinâmico |
| ------ | ------ |
|Revisão, inspeção e análise estática dos artefatos     |  Necessita que o software     |
|Qualquer documento do projeto pode ser avaliado desta forma      | É o mais utilizado no mercado        |
|Custo mais em conta | Custo tende a ser mais alto


# Teste = Qualidade

<h1>![Screenshot_1](https://github.com/felipefrantzpicasso/ProjetoGit/assets/134324945/3cd1c078-7e80-437c-8d58-609ef8fc775d)</h1>


# Erro - Defeito - Falha


| Erro  ❌ | Defeito  ⚠️| Falha ☠  |
| ------ | ------ | ------ |
| Pode ser cometido em qualquer fase do desenvolvimento de software | Resultado do erro cometido | Um evento no qual um componente não executa uma função necessária dentro dos limites especificados |

![Screenshot_2](https://github.com/felipefrantzpicasso/ProjetoGit/assets/134324945/89c8d782-4ac8-4ac2-9224-a4157941708e)


# Atividades e Tarefas de Teste


• Planejamento do teste - O processo de teste envolve a definição de propósitos, abordagem e escopo, a criação de um plano detalhado com cronograma, recursos necessários e exceções, a seleção de métricas, como relatórios diários, e a determinação do nível de detalhamento necessário;

• Análise do teste - Estabelecer os critérios de avaliação relacionados à base de testes examinada na etapa anterior;

• Modelagem do teste - Nesse estágio, é necessário desenvolver os casos de teste com base nas condições de teste identificadas anteriormente, priorizar o conjunto de casos, identificar os dados de teste e projetar o ambiente de teste;

• Implementação do teste - Durante a implementação do teste, é necessário criar os scripts de teste automatizados, criar a suíte de testes e preparar os dados de teste, garantindo seu correto carregamento no ambiente de teste. 

• Execução do teste - Aqui os conjuntos de testes são executados de
acordo com o cronograma de execução;

• Conclusão do teste - Para garantir a qualidade do produto, é essencial ter métricas consolidadas para encerrar os testes, lançar o     software, fechar todos os relatórios de defeitos, analisar lições aprendidas e aprimorar a maturidade dos processos;

<h2 >![Screenshot_4](https://github.com/felipefrantzpicasso/ProjetoGit/assets/134324945/4b910ad5-e0ca-46d9-84ca-8686451c5cda)
</h2>


# Atividade MasterClass 

Como gerar mais qualidade nos projetos?

• Testes de software;

• Avaliação dos requisitos do cliente;


# Assuntos discutidos em grupo:

• Bug no sistema de cupons da amazon;

• Bug no Carrefour fazendo a empresa vender produtos por um valor muito abaixo.

# links:

https://olhardigital.com.br/2022/01/26/pro/possivel-bug-na-amazon-faz-consumidores-comprarem-quase-de-graca/

https://itsfoss.com/a-floating-point-error-that-caused-a-damage-worth-half-a-billion/


# Geral

No dia 4, pude aprender sobre todas as etapas e fundamentos de testes em projetos, além de compreender a importância do testador fornecer feedback e discutir as falhas observadas de forma amigável e construtiva.








