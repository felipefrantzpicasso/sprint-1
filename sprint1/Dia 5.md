## Dia 5

# Fundamentos do teste de software

Ciclo do desenvolvimento e ciclo de teste:

O ciclo de teste ocorre simultaneamente ao ciclo de desenvolvimento, contribuindo para o valor do projeto. Ao realizar testes antecipados, é possível identificar erros antes da implantação, resultando em entregas de maior qualidade e quantidade.

<h1> ![ciclo](https://github.com/felipefrantzpicasso/ProjetoGit/assets/134324945/8327c6f9-653d-46df-8ad3-4183499908bf) </h1>


# Pirâmide de testes

Exibe os níveis dos testes:

<h1> ![piramide 2](https://github.com/felipefrantzpicasso/ProjetoGit/assets/134324945/6ea3a94b-df34-4bf8-98dd-b047cc292782)
 </h1>


| Pirâmide | Complemento |
| ------ | ------ |
| E2E |Realiza a verificação do desempenho da menor parte do código que pode ser testada na aplicação, sem necessitar da interação com outras partes do código.|
| Integration |Realiza a verificação operação correta dos módulos integrados formando unidades funcionais.|
| Unit |Reproduzem um ambiente real da aplicação por meio de simulações.|